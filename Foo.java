import java.lang.*;
public class Foo {
    public static void main(String[] args) {
        int i = 10; 				//iconst_m1 ; istore_1 ; iload_1  (Schreiben, Speichern, Laden)
        if (i < 0) {				//ifge (checkt auf 0!!!) 
            i = 0;					//iconst_0; istore_1; iload_1 (Schreiben, Speichern, Laden)
        }
        long j = 1;
        char c = 'c';
        String s = "hello";
        if (s !=null)
        foo(i,j,c);						//invokestatic  #2 --> Methode wird vorher unter #2 abgespeichert.
    }								//return
    public static void foo(int i, long j, char c) {}
}
