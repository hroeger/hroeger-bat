//package org.opalj
//package br
//package analyses //packages aufbrechen. So spart man sich imports.
//
//import java.net.URL
//import ai.project.CallGraphFactory
//import ai.project.CHACallGraphAlgorithmConfiguration
//
///**
// * @Author Arne Lottmann, Henriette Röger
// * Entry point for treatment analysis. Goes upwards along the call-stack starting at some native method to check for parameter treatments
// */
//object TreatmentAnalysis extends AnalysisExecutor {
//    val analysis = new OneStepAnalysis[URL, BasicReport]  { //Grundgerüst, dass jeder AnalysisExecutor benötigt.
//        override val description = "Treatment Analyses "
//
//        def analyze(project: Project[URL], parameters: Seq[String] = List.empty) = {
//            /*native method*/
//            val classFile = project.classFile(ObjectType("sun/java2d/opengl/OGLRenderQueue")).get // Option  // Path to ClassFile (=Object Type) the native method in question belongs to 
//            val method = classFile.findMethod("flushBuffer", MethodDescriptor(IndexedSeq(LongType, IntegerType), VoidType)).get // Option //Store the native method in "val method"
//
//            /*call graph*/
//            val entryPoints = CallGraphFactory.defaultEntryPointsForLibraries(project) //take some default entry points to start the call graph creation from
//            val callgraph = CallGraphFactory.create(project, entryPoints, new CHACallGraphAlgorithmConfiguration) //build  callgraph based on entrypoints
//
//            val report: String = (for {
//                (caller @ MethodWithBody(body), pc) ← callgraph.callGraph.calledBy(method) //pattern Matching + "caller @" referenz auf die Methode, die ich matche. 
//                //calledBy(method) returns list of all callers
//                //pc is counter where method is called
//            } yield {
//                val callingInstruction = body.associateWithIndex.find(_._1 == pc).get._2 //gibt instruktion, die flashBuffer Aufruf macht, zurück. //yield: return of for-loop
//                //                body.findSequence(windowSize)(f) 
//                caller.toJava //list with all callers having a body.
//            }).mkString("\n") //finish val report
//            BasicReport(report)
//        }
//    }
//}