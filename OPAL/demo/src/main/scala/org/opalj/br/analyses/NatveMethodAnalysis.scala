//
///* License (BSD Style License):
// * Copyright (c) 2009 - 2013
// * Software Technology Group
// * Department of Computer Science
// * Technische Universität Darmstadt
// * All rights reserved.
// *
// * Redistribution and use in source and binary forms, with or without
// * modification, are permitted provided that the following conditions are met:
// *
// *  - Redistributions of source code must retain the above copyright notice,
// *    this list of conditions and the following disclaimer.
// *  - Redistributions in binary form must reproduce the above copyright notice,
// *    this list of conditions and the following disclaimer in the documentation
// *    and/or other materials provided with the distribution.
// *  - Neither the name of the Software Technology Group or Technische
// *    Universität Darmstadt nor the names of its contributors may be used to
// *    endorse or promote products derived from this software without specific
// *    prior written permission.
// *
// * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// * POSSIBILITY OF SUCH DAMAGE.
// */
//package org.opalj
//package br
//package analyses
//
//import java.net.URL
//import scala.collection.mutable.HashSet
//
///**
// * extracts native methods 
// * @author Michael Eichberg, Henriette Röger
// */
//object NatveMethodsAnalysis extends AnalysisExecutor {
//
//    val analysis =  new OneStepAnalysis[URL, BasicReport]  {
//
//        override def description: String = "Counts the number of native methods."
//
//        /**
//         * returns true if a methods signature contains the given data type
//         */
//        def filterByDataType(tmpMethod: Method, dataType: FieldType): Boolean = {
//            return tmpMethod.descriptor.parameterTypes.contains(dataType) //Check if the paramList contains the selected dataType
//        }
//
//        def analyze(project: Project[URL], parameters: Seq[String] = List.empty) = {
//
//            val nativeMethods: Iterable[(ObjectType, Method, Option[URL])] =
//                for (
//                    classFile ← project.classFiles;
//                    method ← classFile.methods if method.isNative & method.descriptor.parametersCount > 0 //check only natives that get any params at all
//                ) yield (classFile.thisType, method, project.source(classFile.thisType))
//            //3. wert ist pfad ins jar-file.
//            /*
//             * returns list with native Methods filtered by parameter type.
//             */
//            //  val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, "int"))
//            //  val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, "long"))
//            //  val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, "java.lang.String"))
//            //  val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, "float"))
//            //  val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, "boolean"))
//            //   val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, "int[]"))
//
//            /**
//             * ************* Läuft über alle Datentypen und speichert das Ergebnis als eine Liste (weil flatMap) in filtered. :)
//             * val filtered = List(IntegerType, ByteType).flatMap { myType =>
//             * nativeMethods filter (x => filterByDataType(x._2, myType))
//             * }
//             *
//             * ******************
//             */
//            //            val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, ArrayType(ByteType)))
//            val filteredMethods = nativeMethods filter (x ⇒ filterByDataType(x._2, ObjectType("sun/java2d/SurfaceData")))
//            val filteredMethodsOutput = filteredMethods map { e ⇒
//                val (t, m, p) = e //t = class, m = method, p = path (inkl. jar file)
//                //shorten the classpath for better readability
//                val pString = p.toString()
//                val pShort = pString.substring(82)
//
//                /*
//                 * returns list with all methods with selected parameter type and whole classpath.
//                 */
//                pShort+"\t"+t.toJava+"\t"+m.toJava //t is class, m is method
//
//                /*
//                 * returns list of classes with selected data type and parameter counter
//                 */
//                //t.toJava+"\t"+m.descriptor.parametersCount+"\t"+m.toJava //t is class, m is method
//            }
//            //                for(
//            //                entry <- nativeMethods; //select one entry
//            //                val paramList = entry._2 if entry._2.descriptor.parameterTypes.mkString().contains("LongType")// map{_.toJava}
//            //               // if paramList.contains(dataType)
//            //              //  filteredMethod 
//            //                )yield (paramList)
//            //   
//
//            /*
//             * returns a list with all parameter types used in native methods
//             */
//            val nativeMethodsAsTSV = nativeMethods map { e ⇒
//                val (t, m, p) = e
//
//                //  t.toJava+"\t"+m.descriptor.parametersCount+"\t"+m.toJava //t is class, m is method
//                //  val tmp =  m.descriptor.parameterTypes.mkString("\n") //store all params in a string
//                //   parameterString.+(tmp)
//                //   parameterString.+(m.descriptor.parameters.parameterTypes)
//                //   val javaList = m.descriptor.parameterTypes.map { _.toJava }
//                val javaList = m.descriptor.parameterTypes
//                javaList.mkString("\n") //return! //returns a list of all parameters used.
//                //    m.descriptor.parameterTypes.toSet
//
//                //        m.descriptor.parameterTypes
//            }
//            //             var tmp = new scala.collection.immutable.HashSet()
//            //             for (
//            //                     parameterSet←nativeMethodsAsTSV
//            //                     tmp.++parameterSet
//            //                     )
//            //                 
//            //            val paramList = nativeMethods map { e ⇒
//            //                val (t, m) = e
//            //                m.descriptor.parameterTypes //enthält vektoren mit parametern
//            //            }
//            //  util.writeAndOpenDesktopApplication(parameterString, "NativeMethods", ".tsv.txt")
//            /*
//            * create output
//            */
//            util.writeAndOpen(filteredMethodsOutput.mkString("\n"), "NativeMethods", ".tsv.txt")
//            // util.writeAndOpenDesktopApplication(nativeMethodsAsTSV.mkString("\n"), "NativeMethods", ".tsv.txt")
//            //               val nativeMethods =
//            //                for (
//            //                    classFile ← project.classFiles;
//            //                    method ← classFile.methods if method.isNative
//            //                ) yield classFile.thisType.toJava+"{ "+method.toJava+" }"
//
//            //            val nativeMethodsAsString = nativeMethods.mkString("\n\t", "\n\t", "\n")
//            //output in txt datei
//            //            util.writeAndOpenDesktopApplication(nativeMethodsAsString, "NativeMethods", ".txt")
//
//            BasicReport(
//                //output
//                nativeMethods.size+" native methods found." //+nativeMethodsAsString
//
//            )
//        }
//    }
//}