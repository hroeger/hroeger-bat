/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai

import java.net.URL
import org.opalj.br.analyses.{ OneStepAnalysis, Analysis, AnalysisExecutor, BasicReport, Project, SomeProject }
import org.opalj.br.Method
import org.opalj.br.{ ObjectType, IntegerType }
import scala.language.existentials
import org.opalj.br.FieldType
import org.opalj.ai.project.CallGraph
import scala.collection.parallel.immutable.ParIterable
import org.opalj.ai.domain.l1.ReferenceValues
import org.opalj.util.Answer
import org.opalj.br.ArrayType
import org.opalj.br.{ ReferenceType, ObjectType, IntegerType }
import org.opalj.br.ClassFile
import org.opalj.ai.domain.MethodCallResults
import org.opalj.ai.domain.l0.RecordMethodCallResults
import scala.util.Random
import org.opalj.ai.domain.l2.PerformInvocations
import scala.collection.immutable.SortedSet
import scala.collection.immutable.HashMap
import scala.collection.immutable.VectorIterator
import org.opalj.br.UpperTypeBound
/**
 * Demonstrates how to identify if an integer value that is passed to a native
 * method is always bounded. - AnalysisClass for Methods with IntegerValues as parameters
 *
 * @author Henriette Röger
 */
object NativeMethodsIntegerRangeAnalysis extends AnalysisExecutor {

    class CoordinatingDomain(
        override val project: Project[java.net.URL])
            extends CoRelationalDomain
            with domain.DefaultDomainValueBinding
            with domain.ThrowAllPotentialExceptionsConfiguration
            with domain.l1.DefaultLongValues
            with domain.l0.DefaultTypeLevelFloatValues
            with domain.l0.DefaultTypeLevelDoubleValues
            with domain.l0.TypeLevelFieldAccessInstructions
            with domain.l0.SimpleTypeLevelInvokeInstructions
            with domain.l1.DefaultReferenceValuesBinding
            //        with domain.l1.DefaultIntegerSetValues
            with domain.l1.DefaultIntegerRangeValues
            with domain.DefaultHandlingOfMethodResults
            with domain.IgnoreSynchronization
            with domain.TheProject
            with domain.ProjectBasedClassHierarchy
            with domain.l0.DefaultPrimitiveValuesConversions
            with RecordMethodCallResults {
        override def maxCardinalityOfIntegerRanges: Long = 16l //set to 4100 for integer range analysis.
    }
    class AnalysisDomain(
        override val project: Project[java.net.URL],
        val method: Method)
            extends CoRelationalDomain
            with domain.DefaultDomainValueBinding
            with domain.ThrowAllPotentialExceptionsConfiguration
            with domain.l1.DefaultLongValues
            with domain.l0.DefaultTypeLevelFloatValues
            with domain.l0.DefaultTypeLevelDoubleValues
            with domain.l0.TypeLevelFieldAccessInstructions
            with domain.l0.TypeLevelInvokeInstructions
            with domain.l1.DefaultReferenceValuesBinding
            with domain.l1.DefaultIntegerRangeValues
            with domain.DefaultHandlingOfMethodResults
            with domain.IgnoreSynchronization
            with domain.TheProject
            with domain.TheMethod
            with domain.ProjectBasedClassHierarchy
            with domain.l0.DefaultPrimitiveValuesConversions
            with domain.l1.MaxArrayLengthRefinement
            with RecordMethodCallResults {
        override def maxCardinalityOfIntegerRanges: Long = 16l //set to 4100 for integer range analysis.
    }

    val analysis = new OneStepAnalysis[URL, BasicReport] {

        override def title: String =
            "Calls of native methods"

        override def description: String =
            "Identifies treatments on native method parameters."

        /**
         * starts an analysis based on the template
         * @param title: title of analysis
         * @param description: description of analysis
         * @param domainValueMatching : matcher that gets the result of the abstract interpretation for one method, the program counter to look at and the stack index and checks whether a requested domainValue is to be found on the operands stack
         * @param nativeMethodsFilter: filters the list of native methods to return only those that contain a desired fieldType
         * @param buildIndexList: returns a list of integers = positions of the selected field type in the parameter list of the given method
         * @param resultsToOutput: method that gets a list of collected parameter interpretations and writes the respective vector-output for the tsv-file.
         * @param title: Analysis title used for output documents.
         */
        def myAnalysis(
            theProject: Project[URL],
            parameters: Seq[String],
            domainValueMatching: (AIResult { val domain: AnalysisDomain }, PC, Int, InterpretedCallPath) ⇒ Unit,
            nativeMethodsFilter: (Method) ⇒ Boolean,
            buildIndexList: (Method) ⇒ IndexedSeq[Int],
            resultsToOutput: (List[ParameterInterpretation]) ⇒ List[String],
            outputTitle: String): Unit = {
            println("Calculating CallGraph")
            // val project.ComputedCallGraph(callGraph, /*we don't care about unresolved methods etc. */ _, _) =
            //     theProject.get(project.VTACallGraphKey)
            val project.ComputedCallGraph(callGraph, /*we don't care about unresolved methods etc. */ _, _) =
                theProject.get(project.CHACallGraphKey)

            println("analyze reference values")
            val calledNativeMethods = //:scala.collection.parallel.ParIterable[Method] =
                //  (theProject.classFiles.par.map { classFile ⇒
                (theProject.classFiles.map { classFile ⇒
                    classFile.methods.filter { m ⇒
                        m.isNative &&
                            nativeMethodsFilter(m) &&
                            /*We want to make some comparisons*/
                            callGraph.calledBy(m).size > 0 //&& //mindestens 1 Caller. 
                        // let's forget about System.arraycopy... causes too much
                        // noise/completely dominates the results
                        // E.g., typical use case: System.arraycopy(A,0,B,0,...)
                        //    !(m.name == "arraycopy" && classFile.thisType == ObjectType("java/lang/System"))
                    }
                }).flatten

            println(
                calledNativeMethods.map(_.toJava).toList.sorted.
                    mkString("Called Native Methods ("+calledNativeMethods.size+"):\n", "\n", ""))

            /**
             * executes abstract analysis and filter operations for all native methods in the iterable object
             */
            def executeAbstractInterpretation(calledNativeMethods: Iterable[Method]): Unit = {
                /*prepare output */
                val unboundedCalls = new java.util.concurrent.atomic.AtomicInteger(0)
                val mutex = new Object
                var resultList: List[InterpretedCallPath] = Nil
                var analysedParametersList: List[InterpretedCallPathOutput] = Nil //returns results as method -> vector with vector values.
                var classifiedCallPaths: List[CallPathClassification] = Nil //container for classifications
                def addCallPathClassification(classification: CallPathClassification) = {
                    mutex.synchronized { classifiedCallPaths = classification :: classifiedCallPaths }
                }
                def addReportEntry(entry: InterpretedCallPath) = {
                    mutex.synchronized { resultList = entry :: resultList }
                }
                def addAnalysisEntry(entry: InterpretedCallPathOutput) {
                    mutex.synchronized { analysedParametersList = entry :: analysedParametersList }
                }
                /**
                 * start Abstract Interpretations
                 */
                println("start abstract interpretation")

                for {
                    nativeMethod ← calledNativeMethods //<= ParIterables
                    // - The last argument to the method is the top-most stack value.
                    // - For this analysis, we don't care whether we call a native instance 
                    //   or native static method
                    parametersCount = nativeMethod.parameterTypes.size
                    //                    parameterIndexes = nativeMethod.parameterTypes.zipWithIndex.collect {
                    //                        //    case (IntegerType, index) ⇒ index //Collect IntegerType parameters
                    //                        // case (_: ReferenceType, index) ⇒ index
                    //                        // case (fieldType., index) ⇒ index
                    //                        case (fType, index) if (fType == fieldType) ⇒ index
                    //}
                    parameterIndexes = buildIndexList(nativeMethod)

                    // For the stack we don't distinguish between computational type
                    // one and two category values; hence, no complex mapping is required.
                    stackIndexes = parameterIndexes.map((parametersCount - 1) - _) //index of analyzed parameters in parameterStack.
                    (caller, pcs) ← callGraph.calledBy(nativeMethod)

                } {
                    val results = if (caller.isPrivate && caller.parameterTypes.size > 0) {
                        val coordinatingDomain = new CoordinatingDomain(theProject)
                        val domain = new AnalysisDomain(theProject, caller)
                        val callerClassFile = theProject.classFile(caller)
                        val adaptedValues = for {
                            (ccaller, pcs) ← callGraph.calledBy(caller) //.toIterable             
                            result = BaseAI(theProject.classFile(ccaller), ccaller, new AnalysisDomain(theProject, ccaller))
                            pc ← pcs.iterable

                        } yield {
                            //adapt relevant domainvalues to targetdomain
                            println(pc+" pc ");
                            if (result.operandsArray(pc) == null) {
                                println("array is null")
                                println(ccaller.toJava()+" "+caller.toJava()+" "+nativeMethod.toJava())
                            } else println(result.operandsArray(pc).head)
                            org.opalj.ai.mapOperandsToParameters(

                                result.operandsArray(pc).take(
                                    caller.descriptor.parametersCount + (if (caller.isStatic) 0 else 1)),
                                caller,
                                domain)
                        }
                        val result = adaptedValues.map(domainValues ⇒ (BaseAI.perform(caller.body.get, domain)(Nil, domainValues))) //one entry in result for each time (each pc) caller is called.
                        result

                        /*   /***test***/
                        val coordinatingDomain = new CoordinatingDomain(theProject) //we need a coordinating Domain for all Callers
                        val domain = new AnalysisDomain(theProject, caller)
                        val callerClassFile = theProject.classFile(caller)
                        val result = List(BaseAI(callerClassFile, caller, domain)) //abstract analysis if caller is not private
                        result */

                    } else {
                        val coordinatingDomain = new CoordinatingDomain(theProject) //we need a coordinating Domain for all Callers
                        val domain = new AnalysisDomain(theProject, caller)
                        val callerClassFile = theProject.classFile(caller)
                        val result = List(BaseAI(callerClassFile, caller, domain)) //abstract analysis if caller is not private
                        result
                    }

                    /**
                     * start evaluation
                     */
                    for { //results = Iterable[AIResults[DomainValue]] ==> all AIResults for one caller!
                        result ← results
                        pc ← pcs
                        reportEntry = InterpretedCallPath(theProject, nativeMethod, caller, pc) //for each callpath...
                        operands = result.operandsArray(pc) //all operands of ONE call. 
                        if operands != null //<= this is practically the only place where a null check is necessary  
                    } {
                        for (stackIndex ← stackIndexes) { //for each element on the operands stack... 
                            domainValueMatching(result, pc, stackIndex, reportEntry)
                        }
                        addReportEntry(reportEntry) //Append entry to output list.
                    }

                }
                println("entry list built")
                /*start analysis of results */
                /*result list: contains all result entries */

                /**
                 * returns whether parameterchecks are not consistent over all callpaths.
                 */
                def isSuspicious(methodInterpretations: List[InterpretedCallPath]): Boolean = {
                    val firstEntry: InterpretedCallPath = methodInterpretations.head //select reference entry
                    var similar = 0
                    for (reportEntry ← methodInterpretations) {
                        val zippedList = firstEntry.entryList.zip(reportEntry.entryList)

                        similar = zippedList.count({ case (x, y) ⇒ x == y }) //check for not similar entries
                        if (similar != firstEntry.entryList.size) {
                            return true
                        }
                    }
                    return false
                }
                /**
                 * +++++++++ parameter level +++++++++++++
                 * helper methods. do not call directly.
                 * takes the caller of a suspicious native method and assigns them to classes based on the parameterInterpretation at parameterPosition.
                 */
                def assignMethodsToClasses(listOfCallPaths: List[InterpretedCallPath], parameterPosition: Int): scala.collection.mutable.Map[ParameterInterpretation, Set[MetaInformationOnCaller]] = {
                    val assignedMethods: scala.collection.mutable.Map[ParameterInterpretation, scala.collection.immutable.Set[MetaInformationOnCaller]] = scala.collection.mutable.Map[ParameterInterpretation, scala.collection.immutable.Set[MetaInformationOnCaller]]()

                    //extract basic information - describes the native method in question.
                    val nativeMethod: Method = listOfCallPaths.head.nativeMethod
                    val project: SomeProject = listOfCallPaths.head.project

                    //get first list of parameters: //toDo: improve performance
                    val entriesPerParameter = listOfCallPaths.map(listEntry ⇒ (listEntry.entryList(parameterPosition), MetaInformationOnCaller(listEntry.project, listEntry.nativeMethod, listEntry.caller, listEntry.callSite, parameterPosition)))

                    //assign all parameter interpretation to a class.
                    for ((entry, metaInformation) ← entriesPerParameter) {
                        addToAssignedMethods(entry, metaInformation)
                    }

                    /**
                     * adds caller by meta information to respective class list or creates a new list if necessary.
                     * Classes are based on parameter interpretations
                     */
                    def addToAssignedMethods(entry: ParameterInterpretation, metaInformation: MetaInformationOnCaller) = {
                        if (assignedMethods.contains(entry)) {
                            val list = assignedMethods.get(entry).get + metaInformation
                            //  list ::: List(metaInformation)
                            assignedMethods.put(entry, list)

                        } else {
                            assignedMethods.put(entry, Set(metaInformation))
                            //   assignedMethods + (entry -> (metaInformation))

                        }
                    }

                    return assignedMethods
                }

                /**
                 * assigns a list of interpreted call paths with meta information (wrapped as reportentry) to equivalence classes.
                 */
                def classify(nativeMethod: Method, listOfCallers: List[InterpretedCallPath]) = {
                    //check for each parameterposition:
                    val parameterCount = listOfCallers.head.entryList.size //#parameters of the required data type
                    val project = listOfCallers.head.project

                    //classification per parameter
                    for (parameterPosition ← 0 to parameterCount - 1) {
                        val callPathClassification = CallPathClassification(project, nativeMethod, (parameterCount - (parameterPosition)), assignMethodsToClasses(listOfCallers, parameterPosition))
                        if (callPathClassification.classifications.size > 1) //add only those parameters to output that really differ.
                            addCallPathClassification(callPathClassification)
                    }
                }

                /*per native method */
                //append native methods with suspicious checkrecord to output
                for (activeMethod ← calledNativeMethods) {
                    val filteredListOfCallPaths = resultList.filter(callPathInterpretation ⇒ callPathInterpretation.nativeMethod.equals(activeMethod)) //get call paths for the active method
                    if (filteredListOfCallPaths.size > 1) { //we're only interested in methods that have multiple call path!
                        //call new method that filters for interesting methods.
                        if (isSuspicious(filteredListOfCallPaths)) { //thus, differences within PIs. (inconsistencies)
                            classify(activeMethod, filteredListOfCallPaths) // add classification of callpath interpretation per parameter to container.
                            val callPathOutputs = filteredListOfCallPaths.map(entry ⇒ new InterpretedCallPathOutput(entry, resultsToOutput(entry.entryList))) //list with parameterInterpretations formatted to output.
                            for (callPathOutput ← callPathOutputs) {
                                addAnalysisEntry(callPathOutput)
                            }
                        }
                    }
                }

                org.opalj.util.writeAndOpen(classifiedCallPaths.mkString("\n"), outputTitle+" Classifications", ".tsv.txt")
                org.opalj.util.writeAndOpen(resultList.mkString("\n"), outputTitle+"Results", ".tsv.txt")
                org.opalj.util.writeAndOpen(analysedParametersList.mkString("\n"), outputTitle+"Vectors", ".tsv.txt")
                println(resultList.size+" vs. "+analysedParametersList.size)
            }

            executeAbstractInterpretation(calledNativeMethods)
        }

        override def doAnalyze(theProject: Project[URL], parameters: Seq[String], isInterrupted: () ⇒ Boolean) = {

            /******************************configurations ******************/
            //call myAnalysis
            def upperTypeBoundDomainValueMatching(result: AIResult { val domain: AnalysisDomain }, pc: PC, stackIndex: Int, reportEntry: InterpretedCallPath): Unit = {
                result.operandsArray(pc)(stackIndex) match {
                    case x: result.domain.DomainReferenceValue if x.upperTypeBound.size > 0 ⇒ { //check if an upper type bound is set.

                        reportEntry.appendEntry(UpperTypeBoundPI(x.upperTypeBound.first))
                    }
                    /*   case x : result.domain.DomainReferenceValue if x.upperTypeBound.size == 0 => //is that check necessary?
                reportEntry.appendEntry(DefaultInterpretation()) */

                    case _ ⇒ reportEntry.appendEntry(DefaultInterpretation()) //no upper bound was set.
                }
            }

            def referenceNullDomainValueMatching(result: AIResult { val domain: AnalysisDomain }, pc: PC, stackIndex: Int, reportEntry: InterpretedCallPath): Unit = {
                //  result.domain = AnalysisDomain
                result.operandsArray(pc)(stackIndex) match {
                    case x: result.domain.DomainReferenceValue if (x.isNull.isNo || x.isNull.isYes) ⇒ { //sure whether null or not. 
                        //   reportEntry.appendEntry(NullEntry(x.isNull))
                        if (x.isNull.isYes) reportEntry.appendEntry(IsNullPI())
                        else reportEntry.appendEntry(IsNotNullPI())
                    }
                    case _ ⇒
                        reportEntry.appendEntry(DefaultInterpretation())
                }
            }

            //used for both integer constants and integer ranges     
            def integerRangeDomainValueMatching(
                result: AIResult { val domain: AnalysisDomain },
                pc: PC,
                stackIndex: Int,
                reportEntry: InterpretedCallPath): Unit = {
                result.operandsArray(pc)(stackIndex) match {
                    case result.domain.IntegerRange(lb, ub) if !(lb == Int.MinValue /*-2147483648 */ && ub == 2147483646) && !(lb == -2147483647 && ub == 2147483647) && lb != ub ⇒ //not int.min AND int.max-1 or int.min +1 AND int.max at the same time.

                        reportEntry.appendEntry(IntegerRangePI(lb, ub)) //real integer range
                    case result.domain.IntegerRange(lb, ub) if lb == ub ⇒ //track constant values as well.
                        reportEntry.appendEntry(IntegerConstantPI(lb))

                    case _ ⇒
                        reportEntry.appendEntry(DefaultInterpretation())
                }
            }

            def integerConstantDomainValueMatching(result: AIResult { val domain: AnalysisDomain },
                                                   pc: PC,
                                                   stackIndex: Int,
                                                   reportEntry: InterpretedCallPath): Unit = {
                result.operandsArray(pc)(stackIndex) match {
                    case result.domain.IntegerRange(lb, ub) if lb == ub ⇒ //not int.min AND int.max-1 at the same time.
                        reportEntry.appendEntry(IntegerConstantPI(lb)) //constant value
                    case _ ⇒
                        reportEntry.appendEntry(DefaultInterpretation())
                }
            }

            //    myAnalysis(theProject, parameters, ... ... ...)
            def referenceNativeMethodsFilter(method: Method): Boolean = {
                method.descriptor.parameterTypes.exists(_.isReferenceType)
            }
            def integerNativeMethodsFilter(method: Method): Boolean = {
                method.descriptor.parameterTypes.exists(_.isIntegerType)
            }

            def referenceUpperTypeBoundResultsToOutput(parameterInterpretations: List[ParameterInterpretation]): List[String] = {
                parameterInterpretations.map(pi ⇒ pi match {
                    case UpperTypeBoundPI(upperTypeBound) ⇒ upperTypeBound.toJava
                    case _                                ⇒ "-"
                })
            }
            def referenceNullResultsToOutput(resultsAsEntries: List[ParameterInterpretation]): List[String] = {
                /*modification to mark relevant methods already during analysis. */
                resultsAsEntries.map(entryType ⇒ entryType match { //produce list of vectors = all caller with all params
                    case IsNullPI()    ⇒ "null"
                    case IsNotNullPI() ⇒ "notnull"
                    case _             ⇒ "-"
                })
            }
            def integerRangeResultsToOutput(resultsAsEntries: List[ParameterInterpretation]): List[String] = {
                resultsAsEntries.map(entryType ⇒ entryType match { //produce list of vectors = all caller with all params
                    //     case IntegerRangeEntry(a, b) if (a != b && (!(a == Int.MinValue && b == Int.MaxValue - 1))) ⇒ a.toString+":"+b.toString() //show only those that contain " real " checks and no constants.
                    case IntegerRangePI(a, b) ⇒ a.toString+" : "+b.toString
                    case _                    ⇒ "-"
                })
            }
            def integerConstantsResultsToOutput(resultsAsEntries: List[ParameterInterpretation]): List[String] = {
                resultsAsEntries.map(entryType ⇒ entryType match { //produce list of vectors = all caller with all params       
                    //   case IntegerRangeEntry(a, b) if a == b ⇒ a.toString //filter for constant values
                    case IntegerConstantPI(value) ⇒ value.toString
                    case _                        ⇒ "-"
                })
            }

            def referenceBuildIndexList(nativeMethod: Method): IndexedSeq[Int] = {
                nativeMethod.parameterTypes.zipWithIndex.collect {
                    case (_: ReferenceType, index) ⇒ index
                }
            }
            def integerBuildIndexList(nativeMethod: Method): IndexedSeq[Int] = {
                nativeMethod.parameterTypes.zipWithIndex.collect {
                    case (IntegerType, index) ⇒ index
                }
            }

            //upperTypeBoundAnalysis
            /*       println("start upper type bound analysis")
            myAnalysis(
                theProject,
                parameters,
                upperTypeBoundDomainValueMatching,
                referenceNativeMethodsFilter,
                referenceBuildIndexList,
                referenceUpperTypeBoundResultsToOutput,
                "referenceUpperTybeBoundReport") */

            //referenceValues Analysis
            /*   println("start reference value null analysis")
            myAnalysis(
                theProject,
                parameters,
                referenceNullDomainValueMatching,
                referenceNativeMethodsFilter,
                referenceBuildIndexList,
                referenceNullResultsToOutput,
                "referenceNullReport") */

            //integerRange Analysis
            println("start integer range analysis")
            myAnalysis(
                theProject,
                parameters,
                integerRangeDomainValueMatching,
                integerNativeMethodsFilter,
                integerBuildIndexList,
                integerRangeResultsToOutput,
                "integerRanges")

            /**
             * build output report
             */
            BasicReport("finished")
        }
    }
}
//new Interpreted Call path for each native Method CALLER (!!)
case class InterpretedCallPath(
        project: SomeProject,
        nativeMethod: Method,
        caller: Method,
        callSite: PC) {
    var entryList: List[ParameterInterpretation] = Nil //list that gets a new entry for each parameter evaluated.

    def appendEntry(entry: ParameterInterpretation) {
        entryList = entry :: entryList
    }
    override def toString = {
        val declaringClassOfNativeMethod = project.classFile(nativeMethod).thisType.toJava
        val declaringClassOfCaller = project.classFile(caller).thisType.toJava
        entryList = entryList.reverse // make sure output is the right way around, not like it is on the stack.
        val entries = (for (entry ← entryList)
            yield entry.toString)
        //output: nativeMethodClass : nativeMethod : callerMethodClass : callerMethod : parameterEvaluation... => im moment werden nur die parameter aufgelistet, deren typen untersucht werden. die anderen werden nicht (auch nicht mit platzhalter) aufgeführt.
        declaringClassOfNativeMethod+" : "+nativeMethod.toJava+"\t"+declaringClassOfCaller+" : "+caller.toJava+"\t"+caller.body.get.lineNumber(callSite).getOrElse("N/A")+"\t"+entries.mkString("\t")
    }
}

/**
 * output of interpreted call path with entry types as strings.
 */
case class InterpretedCallPathOutput(
        reportEntry: InterpretedCallPath,
        evaluatedParameters: List[String]) { //String instead of double, because double property gets lost anyways. string allows for better filter options in excel. 
    override def toString = {
        val declaringClassOfNativeMethod = reportEntry.project.classFile(reportEntry.nativeMethod).thisType.toJava
        val declaringClassOfCaller = reportEntry.project.classFile(reportEntry.caller).thisType.toJava
        val parametersToString = evaluatedParameters.reverse.mkString(" \t ")
        declaringClassOfNativeMethod+" : "+reportEntry.nativeMethod.toJava+"\t"+declaringClassOfCaller+" : "+reportEntry.caller.toJava+"\t"+reportEntry.caller.body.get.lineNumber(reportEntry.callSite).getOrElse("N/A")+"\t"+parametersToString
    }
}
//make a new class for each entry - type (= each new check - operation analyzed for) with a meaningful toString method. Cannot contain a \t! 
abstract class ParameterInterpretation() {
    def toString: String
}
//
//case class NullEntry(
//        answer: Answer) extends EntryType {
//    override def toString = {
//        "NullEntry: "+answer.toString //to be adapted according to requirements.
//    }
//}

case class IsNotNullPI() extends ParameterInterpretation {
    override def toString = {
        "Not null"
    }

}

case class IsNullPI() extends ParameterInterpretation {
    override def toString = {
        "Is null"
    }
}
//for real ranges
case class IntegerRangePI(
        lb: Int,
        ub: Int) extends ParameterInterpretation {
    override def toString = {
        "IntegerRange: "+lb+" to "+ub
    }
}

//for constants
case class IntegerConstantPI(
        value: Int) extends ParameterInterpretation {
    override def toString = {
        "IntegerConstant: "+value
    }

}

case class IntegerSetPI(set: SortedSet[Int]) extends ParameterInterpretation {
    override def toString = { "Integer Set from: "+set.head+" to: "+set.last }
}

case class UpperTypeBoundPI(upperTypeBound: ReferenceType) extends ParameterInterpretation {

    override def toString = {
        "upperTypeBound"+upperTypeBound.toJava
    }
}
//class for default entries when no checks were found
case class DefaultInterpretation() extends ParameterInterpretation {
    override def toString = "-"
}
/**
 * structure to store relevant but basic data about call paths.
 */
case class MetaInformationOnCaller(
        project: SomeProject,
        nativeMethod: Method,
        caller: Method,
        callSite: PC,
        parameterPosition: Int) {
    //prints output for detailed overview.
    override def toString() = {
        val declaringClassOfNativeMethod = project.classFile(nativeMethod).thisType.toJava
        val declaringClassOfCaller = project.classFile(caller).thisType.toJava
        /* declaringClassOfNativeMethod + " : " + nativeMethod.toJava + "\t" + */ declaringClassOfCaller+" : "+caller.toJava+" "+caller.body.get.lineNumber(callSite).getOrElse("N/A")
    }
}
/**
 * container structure contains classification of caller and meta information
 * one instance per parameter.
 */
case class CallPathClassification(
        project: SomeProject,
        nativeMethod: Method,
        parameterCounter: Int,
        classifications: scala.collection.mutable.Map[ParameterInterpretation, Set[MetaInformationOnCaller]]) {
    override def toString = {
        var outputString = project.classFile(nativeMethod).thisType.toJava+" : "+nativeMethod.toJava+" parameter# "+parameterCounter+"\n"
        val sortedClassification = classifications.toSeq.sortWith(_._2.size > _._2.size) //sort classification map by class-size so that biggest class (ie. majority is right) appears on top.
        for ((parameterInterpretation, callPathList) ← sortedClassification) {
            //   outputString = outputString.concat(parameterInterpretation+"\t"+callPathList.mkString(", ")+"\n")
            outputString = outputString.concat(parameterInterpretation+"\t"+callPathList.size+"\n")
        }
        outputString
    }
}

case class CallWithBoundedMethodParameter(
        project: SomeProject,
        nativeMethod: Method,
        parameterIndex: Int,
        caller: Method,
        callSite: PC,
        lowerBound: Int,
        upperBound: Int) {

    override def toString = {
        import Console._
        val declaringClassOfNativeMethod = project.classFile(nativeMethod).thisType.toJava
        val declaringClassOfCaller = project.classFile(caller).thisType.toJava

        "The method "+
            BOLD + declaringClassOfCaller+"{ "+caller.toJava+" }"+RESET+
            " calls in line "+caller.body.get.lineNumber(callSite).getOrElse("N/A")+" the native method "+
            BOLD + BLUE + declaringClassOfNativeMethod+"{ "+nativeMethod.toJava+" }"+RESET+
            " and passes in as the "+parameterIndex+
            ". parameter a bounded value: ["+lowerBound+","+upperBound+"]."
    }

}

case class CallWithNullCheckedParameter(
        project: SomeProject,
        nativeMethod: Method,
        paramterIntex: Int,
        caller: Method,
        callSite: PC,
        isNull: Answer) {

    override def toString = {
        import Console._
        val declaringClassOfNativeMethod = project.classFile(nativeMethod).thisType.toJava
        val declaringClassOfCaller = project.classFile(caller).thisType.toJava

        "The method "+
            BOLD + declaringClassOfCaller+"{ "+caller.toJava+" }"+RESET+
            " calls in line "+caller.body.get.lineNumber(callSite).getOrElse("N/A")+" the native method "+
            BOLD + BLUE + declaringClassOfNativeMethod+"{ "+nativeMethod.toJava+" }"+RESET+
            " with the parameter being Null: "+isNull+"."
    }
}
