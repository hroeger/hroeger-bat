/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai
package analyses

import java.net.URL
import org.opalj.collection.immutable.UIDSet
import org.opalj.br.analyses.{ Analysis, OneStepAnalysis, AnalysisExecutor, BasicReport, SomeProject }
import org.opalj.br.{ ClassFile, Method }
import org.opalj.br.{ ReferenceType }
import org.opalj.ai.Domain
import org.opalj.ai.domain._
import org.opalj.ai.InterruptableAI
import org.opalj.ai.IsAReferenceValue
import org.opalj.util.PerformanceEvaluation.time
import org.opalj.ai.NoUpdate
import org.opalj.ai.SomeUpdate
import org.opalj.br.analyses.Project

/**
 * A shallow analysis that tries to refine the return types of methods.
 *
 * @author Michael Eichberg
 */
object MethodReturnValuesAnalysis {

    def title: String =
        "tries to derive more precise information about the values returned by methods"

    def description: String =
        "Identifies methods where we can – statically – derive more precise return type/value information."

    def doAnalyze(
        theProject: SomeProject,
        isInterrupted: () ⇒ Boolean): Map[Method, Option[MethodReturnValuesAnalysisDomain#DomainValue]] = {

        val methodsWithRefinedReturnValues =
            for {
                classFile ← theProject.classFiles.par
                if !isInterrupted()
                method ← classFile.methods
                originalReturnType = method.returnType
                if originalReturnType.isObjectType
                if theProject.classFile(originalReturnType.asObjectType).map(!_.isFinal).getOrElse(true)
                if method.body.isDefined
                ai = new InterruptableAI[Domain]
                domain = new MethodReturnValuesAnalysisDomain(theProject, ai, method)
                result = ai(classFile, method, domain)
                if !result.wasAborted
                returnedValue = domain.returnedValue
                if returnedValue.isEmpty /* the method does not complete normally */ ||
                    returnedValue.get.asInstanceOf[IsAReferenceValue].upperTypeBound != UIDSet(originalReturnType)
            } yield {
                (method, domain.returnedValue)
            }

        methodsWithRefinedReturnValues.seq.toMap
    }

}

